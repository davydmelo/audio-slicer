cls

del /q "..\feature_extraction\data\WithQueenBeforeRemoval\C0_*.wav"
del /q "..\feature_extraction\data\WithoutQueen\C1_*.wav"
del /q "..\feature_extraction\data\WithQueenAfterRemoval\C2_*.wav"

python slice_random.py -i ../data/C0/26_08_2019.wav -p C0_26_08 -n 3 -N 1000
python slice_random.py -i ../data/C0/27_08_2019.wav -p C0_27_08 -n 3 -N 1000
python slice_random.py -i ../data/C0/29_08_2019.wav -p C0_29_08 -n 3 -N 1000
python slice_random.py -i ../data/C0/30_08_2019.wav -p C0_30_08 -n 3 -N 1000

move C0_*.wav ../feature_extraction/data/WithQueenBeforeRemoval

python slice_random.py -i ../data/C1/02_09_2019.wav -p C1_02_09 -n 3 -N 1000
python slice_random.py -i ../data/C1/03_09_2019.wav -p C1_03_09 -n 3 -N 1000
python slice_random.py -i ../data/C1/04_09_2019.wav -p C1_04_09 -n 3 -N 1000
python slice_random.py -i ../data/C1/05_09_2019.wav -p C1_05_09 -n 3 -N 1000
python slice_random.py -i ../data/C1/06_09_2019.wav -p C1_06_09 -n 3 -N 1000

move C1_*.wav ../feature_extraction/data/WithoutQueen

python slice_random.py -i ../data/C2/10_09_2019.wav -p C2_10_09 -n 3 -N 1000
python slice_random.py -i ../data/C2/11_09_2019.wav -p C2_11_09 -n 3 -N 1000
python slice_random.py -i ../data/C2/12_09_2019.wav -p C2_12_09 -n 3 -N 1000
python slice_random.py -i ../data/C2/13_09_2019.wav -p C2_13_09 -n 3 -N 1000

move C2_*.wav ../feature_extraction/data/WithQueenAfterRemoval