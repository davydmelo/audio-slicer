import wave
import getopt
import sys

def main(argv):
    filename = ""
    n_seconds = 0
    n_slices = 0
    prefix = ""

    try:
        opts, _ = getopt.getopt(argv,"hi:n:N:p:")
    except getopt.GetoptError:
        print('slice.py -i <input_file> -n <seconds_by_slice> -p <prefix>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('slice.py -i <input_file> -n <seconds_by_slice> -p <prefix>')
            sys.exit()
        elif opt in ("-i"):
            filename = arg
        elif opt in ("-p"):
            prefix = arg
        elif opt in ("-N"):
            n_slices = int(arg)
        elif opt in ("-n"):
            n_seconds = int(arg)

    waveread = wave.open(filename)

    rate = waveread.getframerate() # Taxa de amostragem
    frames = waveread.getnframes() # Numero de amostras
    duration = frames / (float(rate))

    total_of_samples = 0

    print("Arquivo de entrada: ", filename)
    print("Quantidade de canais:", waveread.getnchannels())
    print("Bits/amostra:", waveread.getsampwidth() * 8)
    print("Taxa de amostragem: ", rate)
    print("Numero de amostras: ", frames)
    print("Duracao do audio (s): ", duration)

    if (n_slices != 0):
        slice_range = range(1, n_slices + 1)
    else:
        slice_range = range(1, int(frames / (n_seconds * rate)) + 1)

    for i in slice_range:
        waveread.setpos((i-1) * n_seconds * rate)

        wav_slice = waveread.readframes(n_seconds * rate)

        wavewrite = wave.open(prefix + 'slice' + str(i) + '.wav', mode='wb')
        wavewrite.setframerate(rate)
        wavewrite.setnchannels(1)
        wavewrite.setnframes(n_seconds * rate)
        wavewrite.setsampwidth(2)
        wavewrite.writeframes(wav_slice)
        wavewrite.close()

        total_of_samples += n_seconds * rate

    if (n_slices == 0):
        if (total_of_samples < frames):
            wav_slice = waveread.readframes(frames - total_of_samples)

            wavewrite = wave.open(prefix + 'slice' + str(i+1) + '.wav', mode='wb')
            wavewrite.setframerate(rate)
            wavewrite.setnchannels(1)
            wavewrite.setnframes(frames - total_of_samples)
            wavewrite.setsampwidth(2)
            wavewrite.writeframes(wav_slice)
            wavewrite.close()

        total_of_samples += frames - total_of_samples

    waveread.close()

if __name__ == "__main__":
    main(sys.argv[1:])