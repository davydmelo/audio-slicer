import wave
import getopt
import sys
import random

def main(argv):
    filename = ""
    n_seconds = 0
    n_slices = 0
    prefix = ""

    try:
        opts, _ = getopt.getopt(argv,"hi:n:p:N:")
    except getopt.GetoptError:
        print('slice_random.py -i <input_file> -n <seconds_by_slice> -N <number_of_slices> -p <prefix>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('slice_random.py -i <input_file> -n <seconds_by_slice> -N <number_of_slices> -p <prefix>')
            sys.exit()
        elif opt in ("-i"):
            filename = arg
        elif opt in ("-p"):
            prefix = arg
        elif opt in ("-N"):
            n_slices = int(arg)
        elif opt in ("-n"):
            n_seconds = int(arg)

    waveread = wave.open(filename)

    rate = waveread.getframerate() # Taxa de amostragem
    frames = waveread.getnframes() # Número de amostras
    duration = frames / (float(rate))

    print("Arquivo de entrada: ", filename)
    print("Quantidade de canais:", waveread.getnchannels())
    print("Bits/amostra:", waveread.getsampwidth() * 8)
    print("Taxa de amostragem: ", rate)
    print("Número de amostras: ", frames)
    print("Duração do áudio (s): ", duration)

    intervals = get_random_slices(n_slices, n_seconds * rate, frames)

    print(len(intervals))

    count = 0
    for i in intervals:
        waveread.setpos(i[0])
        wav_slice = waveread.readframes(n_seconds * rate)

        wavewrite = wave.open(prefix + 'slice' + str(count + 1) + '.wav', mode='wb')
        wavewrite.setframerate(rate)
        wavewrite.setnchannels(1)
        wavewrite.setnframes(n_seconds * rate)
        wavewrite.setsampwidth(2)
        wavewrite.writeframes(wav_slice)
        wavewrite.close()

        count += 1

    waveread.close()

def get_random_slices(n_slices, slice_size, data_size):

    ns = 0
    intervals = []

    while ns < n_slices:
      
        n = 0
        while n != len(intervals) or n == 0:
            a = random.randint(0,data_size)
            b = a + slice_size

            if (b > data_size):
                continue

            if (len(intervals) == 0):
                intervals.append((a,b))
                ns += 1
                if (n_slices > 1):
                    continue
                else:
                    return intervals
        
            for e in intervals:
                if ((a >= e[0] and a <= e[1]) or (b >= e[0] and b <= e[1])):
                    print(">>>descartado", a, b, e[0], e[1])
                    n = 0
                    break
                else:
                    n += 1

        intervals.append((a,b))
        print(a,b)
        ns += 1

    return intervals

if __name__ == "__main__":
    main(sys.argv[1:])

    
    