# audio-slicer
Scripts Python para recortar arquivos de áudio WAV.

**Uso:**  
Fatiamento sequencial: slice.py -i <input_file> -n <seconds_by_slice> -p <prefix>
Fatiamento aleatório: slice_random.py -i <input_file> -n <seconds_by_slice> -N <number_of_slices> -p <prefix>'

**Ajuda:**  
python slice.py -h
python slice_random.py -h